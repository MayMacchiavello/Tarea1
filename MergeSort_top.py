import time
import random

def mergeSort(alist):
    if len(alist)>1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k]=lefthalf[i]
                i=i+1
            else:
                alist[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < len(lefthalf):
            alist[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < len(righthalf):
            alist[k]=righthalf[j]
            j=j+1
            k=k+1

arreglo = []
archivo = open("datos7.txt", "r")
for linea in archivo:
    b=int(linea)
    arreglo.append(b)
archivo.close()

tiempo_inicial=time.time()
mergeSort(arreglo)

#x = input("Ingresar el ranking a buscar: ")
x=random.randint(1,6400000)
j=0
if x<len(arreglo):
    while j<x:
        #print "El %d termino es: %d " % (j+1,arreglo[j])
        j=j+1
else:
    print "Fuera de rango"

tiempo_final=time.time()
tiempo_ejecucion=tiempo_final-tiempo_inicial
#print "Se demora %f segundos en buscar el %d-esimo termino." % (tiempo_ejecucion,x)
print tiempo_ejecucion